// var input = document.getElementById("m_quicksearch_input");
// input.addEventListener("keyup", function(event) {
// 	var q = $(this).val();
//   if (event.keyCode === 13) {
//    	this.form.submit();
//   }
// });
function searchBtn(){
	$(document).on('keyup', '#m_quicksearch_input1', function(e) {
		if (e.keyCode === 13) {
			$(this).parents('form').submit();
		}
	});
}
var daterangepickerInit = function() {

	if ($('#m_dashboard_daterangepicker').length == 0) {
		return;
	}
	var picker = $('#m_dashboard_daterangepicker');
	var start = moment($('#daterangeform input[name=s]').val(), 'YYYY-MM-DD');
	var end = moment($('#daterangeform input[name=e]').val(), 'YYYY-MM-DD');
//	var start = moment();
//	var end = moment();

	var title = '';
	var range = '';

	if (end.format('YYYY-MM-DD') == start.format('YYYY-MM-DD') && start.format('YYYY-MM-DD') == moment().format('YYYY-MM-DD')) {
		title = 'Today:';
		range = start.format('MMM D');
	} else if (end.format('YYYY-MM-DD') == start.format('YYYY-MM-DD') && start.format('YYYY-MM-DD') == moment().subtract(1, 'days').format('YYYY-MM-DD')) {
		title = 'Yesterday:';
		range = start.format('MMM D');
	} else {
		range = start.format('MMM D, YYYY') + ' - ' + end.format('MMM D, YYYY');
	}
	var maxDate = moment();

	picker.find('.m-subheader__daterange-date').html(range);
	picker.find('.m-subheader__daterange-title').html(title);

	function cb(start, end, label) {
		var title = '';
		var range = '';

		if ((end - start) < 100) {
			title = 'Today:';
			range = start.format('MMM D');
		} else if (label == 'Yesterday') {
			title = 'Yesterday:';
			range = start.format('MMM D');
		} else {
			range = start.format('MMM D') + ' - ' + end.format('MMM D');
		}

		picker.find('.m-subheader__daterange-date').html(range);
		picker.find('.m-subheader__daterange-title').html(title);

		$('#daterangeform input[name=s]').val(start.format('YYYY-MM-DD'));
		$('#daterangeform input[name=e]').val(end.format('YYYY-MM-DD'));
		$('#daterangeform').submit();
	}

	picker.daterangepicker({
		locale: {"format": "DD/MM/YYYY"},
		showDropdowns: true,
		startDate: start,
		endDate: end,
		opens: 'left',
		linkedCalendars: false,
		ranges: {
			'Today': [moment(), moment()],
			'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
			'Last 7 Days': [moment().subtract(6, 'days'), moment()],
			'This Month': [moment().startOf('month'), moment().endOf('month')],
			'Last Month': [moment().subtract(1, 'month'), moment()],
			'Last Year': [moment().subtract(1, 'year'), moment()],
		}
	}, cb);

}

var datepickerInit = function() {

	if ($('#m_dashboard_datepicker').length == 0) {
		return;
	}
	var picker = $('#m_dashboard_datepicker');
	var end = moment($('#dateform input[name=e]').val(), 'YYYY-MM-DD');

	var title = '';
	var range = '';

	if (end.format('YYYY-MM-DD') == moment().format('YYYY-MM-DD')) {
		title = 'Today:';
		range = end.format('MMM D');
	} else if (end.format('YYYY-MM-DD') == moment().subtract(1, 'days').format('YYYY-MM-DD')) {
		title = 'Yesterday:';
		range = end.format('MMM D');
	} else {
		range = end.format('MMM D, YYYY');
	}
	var maxDate = moment();

	picker.find('.m-subheader__daterange-date').html(range);
	picker.find('.m-subheader__daterange-title').html(title);

	function cb(start, end, label) {
		var title = '';
		var range = '';

		if ((end - start) < 100) {
			title = 'Today:';
			range = end.format('MMM D');
		} else if (label == 'Yesterday') {
			title = 'Yesterday:';
			range = end.format('MMM D');
		} else {
			range = end.format('MMM D');
		}

		picker.find('.m-subheader__daterange-date').html(range);
		picker.find('.m-subheader__daterange-title').html(title);

		$('#dateform input[name=e]').val(end.format('YYYY-MM-DD'));
		$('#dateform').submit();
	}

	picker.daterangepicker({
		locale: {"format": "DD/MM/YYYY"},
		singleDatePicker: true,
		showDropdowns: true,
		startDate: end,
		endDate: end,
		opens: 'left'
	}, cb);

}


function formatBook (book) {

	if (book.loading) {
		return book.text;
	}

	var markup = '<div class="clearfix"><span class="float-left"><b>'+book.text+'</b></span><span class="float-right">'+(book.publisher ? book.publisher : "")+'</span></div>';
	markup += '<div class="clearfix"><span class="float-left">'+book.authors+'</span><span class="float-right">'+(book.identification_no ? book.identification_no : "")+'</span></div>';

	return markup;
}

function formatBookSelection (repo) {
	return repo.text;
}

function formatLead (lead) {

	if (lead.loading) {
		return lead.text;
	}

	var markup = '<div class="clearfix"><span class="float-left"><b>'+lead.text+'</b></span></div>';

	return markup;
}

function formatLeadSelection (repo) {
	return repo.text;
}

function formatSubject (subject) {

	if (subject.loading) {
		return subject.text;
	}

	var markup = '<div class="clearfix"><span class="float-left"><b>'+subject.text+'</b></span><span class="float-right">'+subject.type+'</span></div>';

	return markup;
}

function formatSubjectSelection (repo) {
	return repo.text;
}


function formatMagazine (magazine) {

	if (magazine.loading) {
		return magazine.text;
	}

	var markup = '<div class="clearfix"><span class="float-left"><b>'+magazine.text+'</b></span><span class="float-right">'+(magazine.publisher ? magazine.publisher : "")+'</span></div>';
	markup += '<div class="clearfix"><span class="float-left">'+(magazine.frequency ? magazine.frequency : "")+'</span></div>';

	return markup;
}

function formatMagazineSelection (repo) {
	return repo.text;
}

function formatJournal (journal) {

	if (journal.loading) {
		return journal.text;
	}

	var markup = '<div class="clearfix"><span class="float-left"><b>'+journal.text+'</b></span><span class="float-right">'+(journal.publisher ? journal.publisher : "")+'</span></div>';
	markup += '<div class="clearfix"><span class="float-left">'+(journal.identification_no ? journal.identification_no : "")+'</span><span class="float-right">'+(journal.frequency ? journal.frequency : "")+'</span></div>';


	return markup;
}

function formatJournalSelection (repo) {
	return repo.text;
}

function formatNewspaper (newspaper) {

	if (newspaper.loading) {
		return newspaper.text;
	}

	var markup = '<div class="clearfix"><span class="float-left"><b>'+newspaper.text+'</b></span></div>';
	// markup += '<div class="clearfix"><span class="float-left">'+(newspaper.identification_no ? newspaper.identification_no : "")+'</span><span class="float-right">'+(journal.frequency ? journal.frequency : "")+'</span></div>';


	return markup;
}

function formatNewspaperSelection (repo) {
	return repo.text;
}


function formatLibraryBook (library_book) {

	if (library_book.loading) {
		return library_book.text;
	}

	var markup = '<div class="clearfix"><span class="float-left"><b>'+library_book.text+'</b></span><span class="float-right">'+(library_book.publisher ? library_book.publisher : "")+'</span></div>';
	markup += '<div class="clearfix"><span class="float-left">'+library_book.authors+'</span><span class="float-right">Acc.No: '+library_book.accession_number+'</span></div>';

	return markup;
}

function formatLibraryBookSelection (repo) {
	return repo.text;
}

function formatFacutlyMember (faculty_member) {

	if (faculty_member.loading) {
		return faculty_member.text;
	}

	var markup = '<div class="clearfix"><span class="float-left"><b>'+faculty_member.text+'</b></span><span class="float-right">'+(faculty_member.designation ? faculty_member.designation : "")+'</span></div>';
	markup += '<div class="clearfix"><span class="float-left">'+faculty_member.department+'</span><span class="float-right">'+faculty_member.email+'</span></div>';


	return markup;
}

function formatFacutlyMemberSelection (repo) {
	return repo.text;
}


function formatLendingBook (library_book) {

	if (library_book.loading) {
		return library_book.text;
	}

	var markup = '<div class="clearfix"><span class="float-left"><b>'+library_book.text+'</b></span><span class="float-right">'+(library_book.publisher ? library_book.publisher : "")+'</span></div>';
	markup += '<div class="clearfix"><span class="float-left">'+library_book.authors+'</span><span class="float-right">Acc.No: '+library_book.accession_number+'</span></div>';

	return markup;
}

function formatLendingBookSelection (repo) {
	return repo.text;
}

function formatStudent (student) {

	if (student.loading) {
		return student.text;
	}

	var markup = '<div class="clearfix"><span class="float-left"><b>'+student.text+'</b></span>'+(student.university_roll_no ? '<span class="float-right">University Roll No: '+student.university_roll_no+'</span>' : '')+'</div>';
	markup += '<div class="clearfix"><span class="float-left">'+student.program+'</span><span class="float-right">Regn. No: '+student.registration_number+'</span></div>';

	return markup;
}

function formatStudentSelection (repo) {
	return repo.text;
}

// user search
function formatUser (user) {

	if (user.loading) {
		return user.text;
	}

	var markup = '<div class="clearfix"><span class="float-left"><b>'+user.text+'</b></span>'+(user.email ? '<span class="float-right">'+user.email+'</span>' : '')+'</div>';

	return markup;
}

function formatUserSelection (repo) {
	return repo.text;
}

// author search
function formatAuthor (author) {

	if (author.loading) {
		return author.text;
	}

	var markup = author.text;

	return markup;
}

function formatItem (item) {

	if (item.loading) {
		return item.text;
	}
	var markup = '<div class="clearfix"><span class="float-left"><b>'+item.text+'</b></span><span class="float-right"><b>'+item.id+'</b></span></div>';

	return markup;
}

function formatQuestionBank (question_bank) {

	if (question_bank.loading) {
		return question_bank.text;
	}
	var markup = '<div class="clearfix"><span class="float-left"><b>'+question_bank.text+'</b></span><span class="float-right"><b>(Available Questions: '+question_bank.no_of_questions+')</b></span></div>';

	return markup;
}

function formatCategory (category) {
    if (category.loading) {
        return category.text;
    }
    var markup = '<div class="clearfix"><span class="float-left"><b>'+category.text+'</b></span></div>';

    return markup;

}

function formatUserSelection (repo) {
	return repo.text;
}

function disableNumberScroll(){
    // Disable mouse scrolling
    $('input[type=number]').on('mousewheel',function(e){ $(this).blur(); });
    // Disable keyboard scrolling
    $('input[type=number]').on('keydown',function(e) {
        var key = e.charCode || e.keyCode;
        // Disable Up and Down Arrows on Keyboard
        if(key == 38 || key == 40 ) {
            e.preventDefault();
        } else {
            return;
        }
    });
}

function onlyAlphabets() {
    $('.txtNumeric').keyup(function() {
        if (this.value.match(/[^0-9]/g)) {
            this.value = this.value.replace(/[^0-9]/g, '');
        }
    });
    $('.txtdecimal').keyup(function() {
        if (this.value.match(/[^0-9\.]/g)) {
			this.value = this.value.replace(/[^0-9\.]/g, '');
			// var parts = this.value.split('.');
			// if (parts.length > 1 && keycode == 46){
			// 	this.value = this.value.replace(/[^\.]/g, '');
			// }
        }
    });
    $('.txtAlphabet').keyup(function() {
        if (this.value.match(/[^a-zA-Z\s\.]/g)) {
            this.value = this.value.replace(/[^a-zA-Z\s\.]/g, '');
        }
    });
    $('.txtAlphaNumeric').keyup(function() {
        if (this.value.match(/[^a-zA-Z0-9\s\.]/g)) {
            this.value = this.value.replace(/[^a-zA-Z0-9\s\.]/g, '');
        }
    });
}

var fewSeconds = 5;
function submitFormButtonDisable(){
	$('form').submit(function( event ) {
		// alert('submitFormButtonDisable')
		$input_btn = $(this).find("input[type=submit]:focus" );
		$input_btn1 = $(this).find("button[type=submit]:focus");
		$($input_btn).attr('disabled', true);
		$($input_btn1).attr('disabled', true);
		setTimeout(function(){
			$($input_btn).attr('disabled', false);
			$($input_btn1).attr('disabled', false);
		}, fewSeconds*1000);
	});
}
function btnDisableFewSeconds(){
	$('.btn-disable-few-sec').click(function(){
		// alert('btnDisableFewSeconds')
		$btn = $(this);
		$($btn).attr('disabled', true);
		setTimeout(function(){
			$($btn).attr('disabled', false);
		}, fewSeconds*1000);
	});
}

function backButton() {
	$(".btn-back").on("click", function(e){
		e.preventDefault();
		// console.log(window.location);
		if(e.preventDefault()){
			window.history.back();
		}else{
			document.location.href="/";
		}
	});
}

jQuery(document).ready(function() {
	backButton();
	submitFormButtonDisable();
	btnDisableFewSeconds();
	onlyAlphabets();
	daterangepickerInit();
	datepickerInit();
	searchBtn();
	$(".date").datepicker({
		format			: 'yyyy-mm-dd',
		todayHighlight	: true,
	});
    $(".pretty_date").datepicker({
        format			: 'dd/mm/yyyy',
        todayHighlight	: true,
    });
	$(".select").select2({});

	$('#student_selector').select2({
		placeholder			:	'Search Student',
		minimumInputLength	:	1,
		ajax				: 	{
									url				:	base_url+'/students/search',
									dataType		: 	'json',
									data			: 	function (params) {
															return {
																q				: 	params.term,
																status			: 	$(this).attr('data-status'),
																admission		: 	$(this).attr('data-admission')
															};
														},
									cache			: 	true
								},
		escapeMarkup		: 	function (markup) { return markup; },
		templateResult		:	formatStudent,
		templateSelection	: 	formatStudentSelection
	});

	$('#user_selector').select2({
		placeholder			:	'Search User',
		minimumInputLength	:	3,
		ajax				: 	{
									url				:	base_url+'/users/search',
									dataType		: 	'json',
									data			: 	function (params) {
															return {
																q				: 	params.term,
																roles			: 	$(this).attr('data-role')
															};
														},
									cache			: 	true
								},
		escapeMarkup		: 	function (markup) { return markup; },
		templateResult		:	formatUser
	});

	$('#user_selector2').select2({
		placeholder			:	'Search User',
		minimumInputLength	:	3,
		ajax				: 	{
									url				:	base_url+'/users/search',
									dataType		: 	'json',
									data			: 	function (params) {
															return {
																q				: 	params.term,
																roles			: 	$(this).attr('data-role')
															};
														},
									cache			: 	true
								},
		escapeMarkup		: 	function (markup) { return markup; },
		templateResult		:	formatUser
	});

	$('#author_selector').select2({
		placeholder			:	'Search Author',
		minimumInputLength	:	1,
		ajax				: 	{
									url				:	base_url+'/master/authors/search',
									dataType		: 	'json',
									data			: 	function (params) {
															return {
																q			: 	params.term
															};
														},
									cache			: 	true
								}
	});

	$('#book_selector').select2({
		placeholder			:	'Search Book',
		minimumInputLength	:	3,
		ajax				: 	{
									url				:	base_url+'/master/books/search',
									dataType		: 	'json',
									data			: 	function (params) {
															return {
																q			: 	params.term
															};
														},
									cache			: 	true
								},
		escapeMarkup		: 	function (markup) { return markup; },
		templateResult		:	formatBook,
		templateSelection	: 	formatBookSelection
	});

	$('#library_book_selector').select2({
		placeholder			:	'Search Book',
		minimumInputLength	:	3,
		ajax				: 	{
									url				:	base_url+'/library/books/search',
									dataType		: 	'json',
									data			: 	function (params) {
															return {
																q			: 	params.term,
																library_id	: 	$(this).attr('data-library')
															};
														},
									cache			: 	true
								},
		escapeMarkup		: 	function (markup) { return markup; },
		templateResult		:	formatLibraryBook,
		templateSelection	: 	formatLibraryBookSelection
	});

	$('#lending_book_selector').select2({
		placeholder			:	'Search Book',
		minimumInputLength	:	3,
		ajax				: 	{
									url				:	base_url+'/library/returns/search',
									dataType		: 	'json',
									data			: 	function (params) {
															return {
																q			: 	params.term,
																library_id	: 	$(this).attr('data-library')
															};
														},
									cache			: 	true
								},
		escapeMarkup		: 	function (markup) { return markup; },
		templateResult		:	formatLendingBook,
		templateSelection	: 	formatLendingBookSelection
	});

    $('#item_selector').select2({
        placeholder			:	'Search Item',
		minimumInputLength	:	1,
        ajax				: 	{
									url				:	base_url+'/store/item/search',
									dataType		: 	'json',
									data			: 	function (params) {
															return {
																q			: 	params.term,
																categories	: 	$(this).attr('data-categories'),
																is_available_in_store : $(this).attr('data-store')
															};
														},
									cache			: 	true
								},
        escapeMarkup		: 	function (markup) { return markup; },
        templateResult		:	formatItem
    });

    $('#question_bank_selector').select2({
        placeholder			:	'Search Question Bank',
		minimumInputLength	:	1,
        ajax				: 	{
									url				:	base_url+'/question_bank/search',
									dataType		: 	'json',
									data			: 	function (params) {
															return {
																q			: 	params.term,
															};
														},
									cache			: 	true
								},
        escapeMarkup		: 	function (markup) { return markup; },
        templateResult		:	formatQuestionBank
    });


    $('#item_category_selector').select2({
        placeholder			:	'Select Sub-Category',
        minimumInputLength	:	1,
        ajax				: 	{
									url				:	base_url+'/master/item_category/search',
									dataType		: 	'json',
									data			: 	function (params) {
										return {
											q			: 	params.term,
											parent_id	: 	$(this).attr('data-category')
										};
									},
									cache			: 	true
								},
        escapeMarkup		: 	function (markup) { return markup; },
        templateResult		:	formatCategory
    });

    $('#magazine_selector').select2({
		placeholder			:	'Search Magazine',
		minimumInputLength	:	1,
		ajax				: 	{
									url				:	base_url+'/master/magazines/search',
									dataType		: 	'json',
									data			: 	function (params) {
															return {
																q			: 	params.term
															};
														},
									cache			: 	true
								},
		escapeMarkup		: 	function (markup) { return markup; },
		templateResult		:	formatMagazine,
		templateSelection	: 	formatMagazineSelection
	});

	$('#journal_selector').select2({
		placeholder			:	'Search Journal',
		minimumInputLength	:	1,
		ajax				: 	{
									url				:	base_url+'/master/journals/search',
									dataType		: 	'json',
									data			: 	function (params) {
															return {
																q			: 	params.term
															};
														},
									cache			: 	true
								},
		escapeMarkup		: 	function (markup) { return markup; },
		templateResult		:	formatJournal,
		templateSelection	: 	formatJournalSelection
	});

	$('#newspaper_selector').select2({
		placeholder			:	'Search Newspaper',
		minimumInputLength	:	1,
		ajax				: 	{
									url				:	base_url+'/master/newspapers/search',
									dataType		: 	'json',
									data			: 	function (params) {
															return {
																q			: 	params.term
															};
														},
									cache			: 	true
								},
		escapeMarkup		: 	function (markup) { return markup; },
		templateResult		:	formatNewspaper,
		templateSelection	: 	formatNewspaperSelection
	});

	$('#subject_selector').select2({
		placeholder			:	'Search Subject',
		minimumInputLength	:	1,
		ajax				: 	{
									url				:	base_url+'/master/subjects/search',
									dataType		: 	'json',
									data			: 	function (params) {
															return {
																q			: 	params.term
															};
														},
									cache			: 	true
								},
		escapeMarkup		: 	function (markup) { return markup; },
		templateResult		:	formatSubject,
		templateSelection	: 	formatSubjectSelection
	});

	$('#faculty_member_selector').select2({
		placeholder			:	'Search Faculty Member',
		minimumInputLength	:	1,
		ajax				: 	{
									url				:	base_url+'/faculty/search',
									dataType		: 	'json',
									data			: 	function (params) {
															return {
																q				: 	params.term,
																department_id	: 	$(this).attr('data-institute-department'),
																institute_id	: 	$(this).attr('data-institute'),
																institute_ids	: 	$(this).attr('data-institutes'),
																organization_id	: 	$(this).attr('data-organization')
															};
														},
									cache			: 	true
								},
		escapeMarkup		: 	function (markup) { return markup; },
		templateResult		:	formatFacutlyMember,
		templateSelection	: 	formatFacutlyMemberSelection
	});

		$('#faculty_member_department_user_selector').select2({
		placeholder			:	'Search Faculty Member',
		minimumInputLength	:	1,
		ajax				: 	{
									url				:	base_url+'/faculty_department/search',
									dataType		: 	'json',
									data			: 	function (params) {
															return {
																q			: 	params.term,
																department_id	: 	$(this).attr('data-institute-department')
															};
														},
									cache			: 	true
								},
		escapeMarkup		: 	function (markup) { return markup; },
		templateResult		:	formatFacutlyMember,
		templateSelection	: 	formatFacutlyMemberSelection
	});

	$('#competitive_exam_selector').select2({
		placeholder			:	'Search Competitive Exam',
		minimumInputLength	:	1,
		ajax				: 	{
									url				:	base_url+'/master/competitive_exams/search',
									dataType		: 	'json',
									data			: 	function (params) {
															return {
																q			: 	params.term
															};
														},
									cache			: 	true
								}
	});

	$('#lead_selector').select2({
		placeholder			:	'Search lead',
		minimumInputLength	:	3,
		ajax				: 	{
									url				:	base_url+'/leads/search',
									dataType		: 	'json',
									data			: 	function (params) {
															return {
																q			: 	params.term
															};
														},
									cache			: 	true
								},
		escapeMarkup		: 	function (markup) { return markup; },
		templateResult		:	formatLead,
		templateSelection	: 	formatLeadSelection
	});

});
