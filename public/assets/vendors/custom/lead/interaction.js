// $('select[name=interaction_type]').change(function(){
// 	var interaction_type = $(this).val();
// 	if (interaction_type == 3) { //outgoing = 3
// 		// $('select[name=rating]').val(1);
//         // $('select[name=rating]').attr('style', 'pointer-events:none;');

// 	} else{
//         // $('select[name=rating]').attr('style', 'pointer-events:unset;');
// 	}
// });
 
$(function(){
	$('select[name=interaction_type]').change(function(){
		var interaction_type = $(this).val();
		if (interaction_type == 2){ //incoming = 2
			$('select[name=reachable]').val(1);
			$('select[name=reachable]').attr('style', 'pointer-events:none;');

			$('select[name=rating]').val(1);
	        $('select[name=rating]').attr('style', 'pointer-events:unset;');
	        $('select[name=interaction_medium]').html('');
			$('select[name=interaction_medium]').append("<option value='2'>IM</option>");
			$('select[name=interaction_medium]').append("<option value='1'>Phone</option>");
			$('select[name=interaction_medium]').append("<option value='3'>Email</option>");
			$('select[name=interaction_medium]').append("<option value='4'>Campus Visit</option>");
			$('select[name=interaction_medium]').append("<option value='5'>Website Form</option>");
			$('select[name=interaction_medium]').append("<option value='6'>Line</option>");
			$('select[name=interaction_medium]').append("<option value='7'>Whatsapp</option>");
			$('select[name=interaction_medium]').append("<option value='8'>SMS</option>");
			$('select[name=interaction_medium]').append("<option value='9'>Viber</option>");
			$('select[name=interaction_medium]').append("<option value='10'>Facebook Messenger</option>");

		}else if(interaction_type == 3){ //outgoing = 3
			$('select[name=reachable]').val(0);
	        $('select[name=reachable]').attr('style', 'pointer-events:unset;');
			$('select[name=rating]').val(1);
	        $('select[name=rating]').attr('style', 'pointer-events:none;');

			$('select[name=interaction_medium]').html('');
			$('select[name=interaction_medium]').append("<option value='2'>IM</option>");
			$('select[name=interaction_medium]').append("<option value='1'>Phone</option>");
			$('select[name=interaction_medium]').append("<option value='3'>Email</option>");
			$('select[name=interaction_medium]').append("<option value='6'>Line</option>");
			$('select[name=interaction_medium]').append("<option value='7'>Whatsapp</option>");
			$('select[name=interaction_medium]').append("<option value='8'>SMS</option>");
			$('select[name=interaction_medium]').append("<option value='9'>Viber</option>");
			$('select[name=interaction_medium]').append("<option value='10'>Facebook Messenger</option>");
		}
	});
	$('select[name=interaction_type]').trigger('change'); //This event will fire the change event. 
});



$('select[name=reachable]').change(function(){
	var reachable = $(this).val();
	if (reachable == 1) {
		// $(".reachable").show()
        $('select[name=rating]').attr('style', 'pointer-events:unset;');

	} else if(reachable == 0){
		// $(".reachable").hide()
		// $('select[name=rating]').attr('disabled',true);
		$('select[name=rating]').val(1);
        $('select[name=rating]').attr('style', 'pointer-events:none;');
	}
});


// $(function(){
$('select[name=interaction_medium]').change(function(){
	var interaction_medium = $(this).val();
	if (interaction_medium == 3) {
		$('.email').show();
		$('select[name=email]').attr('required', true);
		$('select[name=email]').attr('disabled', false);
		$('.contact_number').hide();
		$('select[name=contact_number]').attr('required', false);
		$('select[name=contact_number]').attr('disabled', true);
	}else if(interaction_medium == 1){
		$('.contact_number').show();
		$('select[name=contact_number]').attr('required', true);
		$('select[name=contact_number]').attr('disabled', false);
		$('.email').hide();
		$('select[name=email]').attr('required', false);
		$('select[name=email]').attr('disabled', true);
	} else{
		$('.contact_number').hide();
		$('.email').hide();
		$('select[name=contact_number]').attr('required', false);
		$('select[name=email]').attr('required', false);
		$('select[name=contact_number]').attr('disabled', true);
		$('select[name=email]').attr('disabled', true);
	}

});
// $('select[name=interaction_medium]').trigger('change'); //This event will fire the change event. 
// });


	$(function(){
	    $('select[name=status]').change(function(){
	      var status = $(this).val();
		if (status == 2|| (status == 3)) {
			$('select[name=lead_case]').attr('required', true);

			$.ajax({
	            type: "GET",
	            url: '/lead/interaction/lead_cases',
	            data: {status: status},
	            success: function(data) {
	            	$("#lead_case").html('');
	            	$("#lead_case").append('<option value="">Select</option>');
	            	$.each(data.results, function(key,value) {
	            		$("#lead_case").append("<option value ="+key+">"+value+"</option>");
					});

	            	$(".lead_case_div").show();
	            	$(".pending_task").hide();
					$('input[name=task_date]').attr('disabled', true);
					$('textarea[name=task_notes]').attr('disabled', true);
					// $(".lead_case_reason").show();
	            }
	        });

		}else if(status == 1){
			$('select[name=lead_case]').attr('required', false);
			$(".lead_case_div").hide();
			$(".pending_task").show();
			$('input[name=task_date]').attr('disabled', false);
			$('textarea[name=task_notes]').attr('disabled', false);
			// $(".lead_case_reason").hide();
		}
		//  else{
		// 	$(".lead_case_div").hide();
		// 	// $(".lead_case_reason").hide();
		// 	$(".pending_task").hide();
		// 	$('input[name=task_date]').attr('disabled', true);
		// 	$('textarea[name=task_notes]').attr('disabled', true);

		// }
	    });
		$('select[name=status]').trigger('change'); //This event will fire the change event. 
	});



	 // var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
  //       $("input[name=phone]").keyup(function(){
  //       	var country_code = $("input[name=country_code]").val();
  //       	var lead_id = $("input[name=lead]").val();
  //       	var phone = $(this).val();
  //           $.ajax({
  //               url: 'add_phone/verify',
  //               type: 'POST',
  //               data: {_token: CSRF_TOKEN, country_code:country_code, phone:phone, lead_id:lead_id},
  //               dataType: 'JSON',
  //               success: function (data) { 
  //               	if (data.status == 'success') {
		// 				$('.add_phone_submit').attr('disabled', false);
  //               	} else {
		// 				$('.add_phone_submit').attr('disabled', true);
  //               	}
				
		// 		console.log(data);
				
  //               		$("#merge_request").html('');
	 //                	if (data.merge_request == true) {
	 //                		// var uri = data.original_lead+'/duplicate_merge/'+data.duplicate_lead;
	 //                		// console.log(data.duplicate_lead);

	 //                		$("#duplicate_lead_id").val(data.duplicate_lead);
		// 					$("#merge_request").html('<a href="#" onclick="return submit_merge_request()" class="btn m-btn m-btn--gradient-from-metal m-btn--gradient-to-accent merge_duplicate">Merge</a>');
	 //                	}
	 //                	// else{
	 //                	// 	$("input[name=duplicate_lead_id]").val('');
	 //                	// }

  //                   $(".error_phone").html(data.msg); 
  //               }
  //           }); 
  //       });





        // email add 
         $("#email").keyup(function(){
        	var lead_id = $("input[name=lead]").val();
        	var email = $(this).val();
            $.ajax({
                url: 'add_email/verify',
                type: 'POST',
                data: {_token: CSRF_TOKEN, email:email, lead_id:lead_id},
                dataType: 'JSON',
                success: function (data) { 
                	if (data.status == 'success') {
						$('.add_email_submit').attr('disabled', false);
                	} else {
						$('.add_email_submit').attr('disabled', true);
                	}
	            		$("#email_merge_request").html('');
	                	if (data.merge_request == true) {
	                		$("#email_duplicate_lead_id").val(data.duplicate_lead);
							$("#email_merge_request").html('<a href="#" onclick="return submit_email_merge_request()" class="btn m-btn m-btn--gradient-from-metal m-btn--gradient-to-accent merge_duplicate">Merge</a>');
	                	}
                    $(".error_email").html(data.msg); 
                }
            }); 
        });




	// clear form on reclick start
	$('.clearPhoneForm').on('click', function(){
	    $('#addPhone').find('form')[0].reset();
	});		

	$('.clearEmailForm').on('click', function(){
	    $('#addEmail').find('form')[0].reset();
	});	

	$('.clearInteractionForm').on('click', function(){
	    $('#addInteraction').find('form')[0].reset();
		$(".contact_information").hide()
		$(".reachable").hide()
	    
	});		
	// clear form on reclick end

	$(".pending_task_date").datepicker({
		format			: 'yyyy-mm-dd',
		todayHighlight	: true,
 		startDate  		: "today",
	});


//otp verify number add
$(document).ready(function(){
    	// $("input[name=phone]").keyup(function(){
	    $(".otp_phone_div").hide();
		$("input[name=otp_phone]").attr('required', false);		
		$('.send_phone_otp_btn').on('click', function(){
		
		var country_code = $("input[name=country_code]").val();
    	var phone = $("#mobile").val();
    	var lead_id = $("input[name=lead]").val();
    	
	        $.ajax({
                url: 'add/new/phone',
                type: 'POST',
                data: {_token: CSRF_TOKEN, country_code:country_code, phone:phone, lead_id:lead_id},
                dataType: 'JSON',
                success: function (data) {
                	if (data.status == 'success') {

                		$(".otp_phone_div").show();    		
                		$(".mobile_div").hide();

                		$("input[name=otp_phone]").attr('required', false);
						$('.submit_phone_btn').attr('disabled', false);
                	} else {
						$('.submit_phone_btn').attr('disabled', true);
                	}
       //          		$("#merge_request").html('');
	      //           	if (data.merge_request == true) {
	      //           		$("#duplicate_lead_id").val(data.duplicate_lead);
							// $("#merge_request").html('<a href="#" onclick="return submit_merge_request()" class="btn m-btn m-btn--gradient-from-metal m-btn--gradient-to-accent merge_duplicate">Merge</a>');
	      //           	}
                    $(".error_phone").html(data.msg);
                }
            });
        });		
	// $('.send_phone_otp_btn').trigger('click');
	});		



	// submit phone
	$('.submit_phone_btn').on('click', function(){
		
		var country_code = $("input[name=country_code]").val();
    	var phone = $("#mobile").val();
    	var lead_id = $("input[name=lead]").val();
    	var otp_phone = $("input[name=otp_phone]").val();
    	
	        $.ajax({
                url: 'store/new/phone',
                type: 'POST',
                data: {_token: CSRF_TOKEN, country_code:country_code, phone:phone, lead_id:lead_id, otp_phone:otp_phone},
                dataType: 'JSON',
                success: function (data) {
                	$("#merge_request").html('');
                	if (data.merge_request == true) {
						$('.submit_phone_btn').attr('disabled', true);
						$('.send_phone_otp_btn').attr('disabled', true);
                		$("#duplicate_lead_id").val(data.duplicate_lead);
						$("#merge_request").html('<a href="#" onclick="return submit_merge_request()" class="btn m-btn m-btn--gradient-from-metal m-btn--gradient-to-accent merge_duplicate">Merge</a>');
                	}

                	if (data.status == 'updated') {
						// window.opener.location.reload(true);
                		location.reload();
                		alert('Mobile Number Updated');
                	} 
                	
                    $(".error_otp_phone").html(data.msg);
                }
            });
        });		

	// submit phone




    	// $('.add_phone_submit').attr('disabled', true);
    	// var country_code = $("input[name=country_code]").val();
    	// var lead_id = $("input[name=lead]").val();
    	// var phone = $(this).val();
     //        $.ajax({
     //            url: 'add_phone/verify',
     //            type: 'POST',
     //            data: {_token: CSRF_TOKEN, country_code:country_code, phone:phone, lead_id:lead_id},
     //            dataType: 'JSON',
     //            success: function (data) {
     //            	if (data.status == 'success') {
					// 	$('.add_phone_submit').attr('disabled', false);
     //            	} else {
					// 	$('.add_phone_submit').attr('disabled', true);
     //            	}
     //            		$("#merge_request").html('');
	    //             	if (data.merge_request == true) {
	    //             		$("#duplicate_lead_id").val(data.duplicate_lead);
					// 		$("#merge_request").html('<a href="#" onclick="return submit_merge_request()" class="btn m-btn m-btn--gradient-from-metal m-btn--gradient-to-accent merge_duplicate">Merge</a>');
	    //             	}
     //                $(".error_phone").html(data.msg);
     //            }
     //        });
     //    });
    // });
//otp verify number add