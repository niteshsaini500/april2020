$(function(){
	$('select[name=medium_type]').change(function(){
		var medium_type = $(this).val();
		if (medium_type == 'sms') {
			$(".mobile_message").show();
			$(".email_message").hide();
			$(".send_message_btn").show();			
			$("select[name=contact_number]").attr('required', true);
			$("textarea[name=message]").attr('required', true);

			$("input[name=title]").attr('required', false);
			$("textarea[name=subject]").attr('required', false);
			$("select[name=email]").attr('required', false);
		} else if(medium_type == 'email') {
			$(".mobile_message").hide();
			$(".email_message").show();
			$(".send_message_btn").show();		
			$("select[name=contact_number]").attr('required', false);
			$("textarea[name=message]").attr('required', false);

			$("input[name=title]").attr('required', true);
			$("textarea[name=subject]").attr('required', true);
			$("select[name=email]").attr('required', true);
		} else {
			$(".mobile_message").hide();
			$(".email_message").hide();
			$(".send_message_btn").hide();	
			$("select[name=contact_number]").attr('required', false);
			$("textarea[name=message]").attr('required', false);
			
			$("input[name=title]").attr('required', false);
			$("textarea[name=subject]").attr('required', false);		
			$("select[name=email]").attr('required', false);
		}
	});
	$('select[name=medium_type]').trigger('change'); //This event will fire the change event. 
});
